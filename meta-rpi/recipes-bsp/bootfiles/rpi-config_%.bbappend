do_deploy_append() {

    # Adafruit touchscreen LCD    
    echo "hdmi_force_hotplug=1" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "hdmi_group=2" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "hdmi_mode=1" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "hdmi_mode=87" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "hdmi_cvt=800 480 60 6 0 0 0" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "framebuffer_depth=24" >> ${DEPLOYDIR}/bcm2835-bootfiles/config.txt
}
