FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRCPV = "${LINUX_VERSION}"

CMDLINE_append = " console=tty0 "

AUFS_FS_MODE = "m"
AUFS_VERSION = "aufs4.4"

require recipes-kernel/linux/aufs-kernel-patches.inc

CMDLINE = "dwc_otg.lpm_enable=0 live=LABEL=live roimg=/live/os.img "

kernel_do_configure_prepend() {

    kernel_configure_variable AUDIT y
    kernel_configure_variable CPU_FREQ_DEFAULT_GOV_POWERSAVE n
    kernel_configure_variable CPU_FREQ_DEFAULT_GOV_ONDEMAND y
    kernel_configure_variable LEGACY_PTYS n   

}
