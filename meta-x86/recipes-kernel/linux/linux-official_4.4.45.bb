require recipes-kernel/linux/linux-official.inc

PR = "r1"

AUFS_VERSION = "aufs4.4"
KBRANCH = "linux-4.4.y"
METABRANCH = "yocto-4.4"
SRCREV_kernel = "4b18b3dca6516a18d03e5dab649bda2998b2afae"
SRCREV_meta = "78a26182e20c0a49c1adda63faa15ccd3f4ecb27"

SRC_URI += "file://common.scc"

SRC_URI_append_baytrail-64 = " file://baytrail-64.scc "

KERNEL_FEATURES_append = " ${@bb.utils.contains("TUNE_FEATURES", "mx32", " cfg/x32.scc", "" ,d)}"
KERNEL_FEATURES_append = " ${@bb.utils.contains("TUNE_FEATURES", "m64", " cfg/x86_64.scc", "" ,d)}"
