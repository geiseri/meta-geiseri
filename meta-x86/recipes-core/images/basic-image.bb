include recipes-core/images/core-image-minimal.bb

inherit ext4live

IMAGE_FEATURES += "ssh-server-dropbear debug-tweaks"

CORE_IMAGE_EXTRA_INSTALL += "\
                             kernel-modules \
                             connman connman-client \
                             systemd-analyze \
                           "

IMAGE_FSTYPES = "ext4 ext4live"
INITRD_IMAGE  = "initramfs-systemd-image"
SYSLINUX_ROOT = "live=/dev/sda2"
APPEND += " roimg=/live/os.img "
ROOTFS = "${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}.ext4live"
LIVE_TYPE = "squashfs-xz"

