
IMAGE_FEATURES += "ssh-server-dropbear splash x11-base"

CORE_IMAGE_EXTRA_INSTALL += "\
                               nodm \
                               kernel-modules linux-firmware \
                               connman connman-client upower udisks2 \
                               qttools-tools qtimageformats-plugins qml-material quest \
                               packagegroup-fonts-truetype-core \
                               mesa-megadriver xserver-xorg-extension-glx xf86-video-intel \
                               systemd-analyze \
                               usbutils pciutils i2c-tools dmidecode \
                            "
VM_ROOTFS_TYPE = "ext4live"
IMAGE_FSTYPES = "ext4live hdddirect"
INITRD_IMAGE_VM  = "initramfs-systemd-image"
ROOT_VM = "live=LABEL=live"
APPEND += "roimg=/live/os.img"
LIVE_TYPE = "squashfs-xz"

inherit core-image
inherit ext4live
inherit image-vm
