VM_ROOTFS_TYPE = "ext4live"
IMAGE_FSTYPES += "ext4live hdddirect"
INITRD_IMAGE_VM  = "initramfs-systemd-image"
ROOT_VM = "live=LABEL=live"
APPEND += "roimg=/live/os.img"
LIVE_TYPE = "squashfs-xz"

inherit ext4live
inherit image-vm 
