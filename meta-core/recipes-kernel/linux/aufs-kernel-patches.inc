AUFS_FS_MODE ?= "m"
AUFS_VERSION ?= "aufs4.1.13+"

def get_aufs_rev(d):
    aufs_srcrevs = {
        'aufs4.0':'f3daf663294ae51cde1105450705a83d7f0abf84',
        'aufs4.1':'65c0180f67bd6592918d8ced1c20c3c336705f61',
        'aufs4.1.13+':'ab6b23008ad54d34fb20d98269d280cb8a971892',
        'aufs4.2':'98d58b75c249bde4e43142037409078f3349f05e',
        'aufs4.3':'2f42a97753132c418d3b7497016f936f7af6ddd8',
        'aufs4.4':'885f7bd95a14d7e18d429cd83ed8c087e858237d',
        'aufs4.5':'80226f98189c5143b92ee6e915afc6c4433cdc79',
        'aufs4.6':'fdc305e1d805be7118927ebea29219a97fcd7744',
        'aufs4.7':'98e87c2035a11b0a90df418ea929ffc2ec83d8a6',
        'aufs4.8':'2be6d49293c74e3cf3c31702fc84cc6ae3dc146e',
        'aufs4.9':'6f44e3b0bd99399516982a04782d78f8dae96600',
    }
    aufs_ver = d.getVar('AUFS_VERSION', True)
    if aufs_ver:
        if aufs_ver in aufs_srcrevs:
            return aufs_srcrevs[aufs_ver]
        else:
            bb.fatal('Not a supported AUFS_VERSION')
    return ''

SRCREV_aufs = "${@get_aufs_rev(d)}"
SRC_URI_append = " git://github.com/sfjro/aufs4-standalone.git;branch=${AUFS_VERSION};protocol=git;destsuffix=aufs;name=aufs "

do_setup_additional_sources() {
  patch -d ${S} -p1  < ${WORKDIR}/aufs/aufs4-base.patch
  if [ -f ${WORKDIR}/aufs/aufs4-mmap.patch ]; then
      patch -d ${S} -p1  < ${WORKDIR}/aufs/aufs4-mmap.patch
  fi
  if [ -f ${WORKDIR}/aufs/aufs4-proc_map.patch ]; then
      patch -d ${S} -p1  < ${WORKDIR}/aufs/aufs4-proc_map.patch
  fi
  patch -d ${S} -p1  < ${WORKDIR}/aufs/aufs4-kbuild.patch
  patch -d ${S} -p1  < ${WORKDIR}/aufs/aufs4-standalone.patch

  cp -r ${WORKDIR}/aufs/Documentation ${S}
  cp -r ${WORKDIR}/aufs/fs ${S}
  if [ -f ${WORKDIR}/aufs/include/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/linux/aufs_type.h ${S}/include/linux/
  fi
  if [ -f ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ${S}/include/uapi/linux/
  fi
}

do_configure_prepend() {
  echo "${@base_conditional('AUFS_FS_MODE', 'm', 'CONFIG_AUFS_FS=m', '',d)}" >> ${B}/.config
  echo "${@base_conditional('AUFS_FS_MODE', 'y', 'CONFIG_AUFS_FS=y', '',d)}" >> ${B}/.config
  echo "CONFIG_AUFS_BRANCH_MAX_511=y" >> ${B}/.config
  echo "CONFIG_AUFS_BR_FUSE=y" >> ${B}/.config
  echo "CONFIG_AUFS_BR_RAMFS=y" >> ${B}/.config
  echo "CONFIG_AUFS_BDEV_LOOP=y" >> ${B}/.config
  echo "CONFIG_AUFS_POLL=y" >> ${B}/.config
  echo "CONFIG_AUFS_SBILIST=y" >> ${B}/.config 
  echo "CONFIG_AUFS_EXPORT=y" >> ${B}/.config
  echo "CONFIG_AUFS_PROC_MAP=y" >> ${B}/.config
  echo "CONFIG_AUFS_SHWH=y" >> ${B}/.config
  echo "CONFIG_AUFS_XATTR=y" >> ${B}/.config
}

addtask setup_additional_sources after do_patch before do_configure
