SUMMARY = "QML Material Design Components"
LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4fbd65380cdd255951079008b364516c"

PV = "0.3.0+gitr${SRCPV}"

DEPENDS = "qtdeclarative"

INSANE_SKIP_${PN} += "dev-so"

FILES_${PN} += "\
                ${libdir}/qt5/qml/*/*.qml \
                ${libdir}/qt5/qml/*/qmldir \
                ${libdir}/qt5/qml/*/*.js \
                ${libdir}/qt5/qml/*/*/*.qml \
                ${libdir}/qt5/qml/*/*/qmldir \
                ${libdir}/qt5/qml/*/*.so* \
                ${libdir}/qt5/qml/*/*.otf \
                ${libdir}/qt5/qml/*/*/*.png \
                ${libdir}/qt5/qml/QtQuick \
               "

FILES_${PN}-dev += "\
                    ${libdir}/qt5/qml/*/*.cpp \
                    ${libdir}/qt5/qml/*/*.h \
                    ${libdir}/qt5/qml/*/*.qrc \
                    ${libdir}/qt5/qml/*/*/*.cpp \
                    ${libdir}/qt5/qml/*/*/*.h \
                    ${libdir}/qt5/qml/*/*/*.qrc \
                    /usr/share/tests/tst_material/tst_material \
                   "
FILES_${PN}-dbg += "\
                     ${libdir}/qt5/qml/*/.debug \
                     /usr/share/tests/tst_material/.debug \ 
                   "

RDEPENDS_${PN} += " \
                     qtdeclarative-qmlplugins \
                     qtdeclarative-plugins \
                     qtquickcontrols-qmlplugins \
                     qtgraphicaleffects-qmlplugins \
                     qtbase-plugins \
                     qtsvg \
                  "
SRC_URI = "git://github.com/papyros/${PN}.git;protocol=http;branch=develop"

SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

inherit qmake5 gitpkgv
