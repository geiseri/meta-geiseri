SUMMARY = "A QML based web browser for small devices"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=c5bcd576364ad6a65c5f7cad73953aab"
DEPENDS = "qtwebengine"
RDEPENDS_${PN} += "qml-material qtwebengine-qmlplugins"

SRC_URI = "git://bitbucket.org/geiseri/quest.git;protocol=http"
SRCREV = "${AUTOREV}"
PV = "1.0+gitr${SRCPV}"

S = "${WORKDIR}/git"

inherit qmake5 gitpkgv

