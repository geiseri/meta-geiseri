SUMMARY = "A digital picture frame for webdav shares"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=c5bcd576364ad6a65c5f7cad73953aab"
DEPENDS = "qtbase qtdeclarative qtquickcontrols"
RDEPENDS_${PN} += "qml-material qtvirtualkeyboard"
SRC_URI = "git://bitbucket.org/geiseri/cloudframe.git;protocol=http"

SRCREV = "${AUTOREV}"
PV = "1.0+gitr${SRCPV}"
PR = "r1"

FILES_${PN} += " \
    /opt/CloudFrame/bin/CloudFrame \
    /etc/xdg \
    "

S = "${WORKDIR}/git"

inherit qmake5 gitpkgv

