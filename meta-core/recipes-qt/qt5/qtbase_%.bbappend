PACKAGECONFIG_DISTRO += " \
    accessibility \
    glib \
    sql-sqlite \
    "

PACKAGECONFIG_FONTS = " \
    freetype \
    fontconfig \
    harfbuzz \
    "

ASNEEDED = ""
