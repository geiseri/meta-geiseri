inherit image_types

IMAGE_DEPENDS_ext4live = "e2fsprogs-native squashfs-tools-native"

LIVE_IMAGE = "${IMGDEPLOYDIR}/${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.ext4live"
LIVE_TYPE ?= "squashfs"

IMAGE_TYPEDEP_ext4live = "${LIVE_TYPE}"
LIVE_ROOTFS ?= "${IMGDEPLOYDIR}/${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${LIVE_TYPE}"

IMAGE_CMD_ext4live () {

        mkdir -p ${WORKDIR}/root/live
        cp ${LIVE_ROOTFS} ${WORKDIR}/root/live/os.img
        dd if=/dev/zero of=${LIVE_IMAGE} seek=${ROOTFS_SIZE} count=0 bs=1024
        mkfs.ext4 -i 4096 -L live -F ${LIVE_IMAGE} -d ${WORKDIR}/root

}

IMAGE_TYPES += "ext4live"
