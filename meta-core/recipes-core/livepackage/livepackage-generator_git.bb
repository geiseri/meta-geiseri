SUMMARY = "A systemd generator for loading packges on a live-boot systems"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = "systemd"

inherit pkgconfig cmake gitpkgv

SRCBRANCH = "master"
SRCREV = "${AUTOREV}"

SRC_URI = "\
             git://bitbucket.org/geiseri/livepackage-generator.git;protocol=https;branch=${SRCBRANCH} \
          "

S = "${WORKDIR}/git"

FILES_${PN} += " \
                   /lib/systemd/system/package@.service \
                   /lib/systemd/system/package@.mount \
                   /lib/systemd/system-generators/livepackage-generator \
               "

