IMAGE_LINGUAS = " "
LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"

IMAGE_FEATURES += "ssh-server-dropbear \ 
    splash \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'x11', '', d)} \
    hwcodecs \
    "

CORE_IMAGE_EXTRA_INSTALL += "\
    packagegroup-baseos \    
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'packagegroup-baseos-x11', '', d)} \
    "
