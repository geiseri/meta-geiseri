SUMMARY = "A systemd generator for live-boot systems"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = "systemd"

inherit cmake pkgconfig

SRCBRANCH = "master"
SRCREV = "b8e5f22579f722dac6c3f944eb18ad707d600c43"

SRC_URI = "\
             git://bitbucket.org/geiseri/liveboot-generator.git;protocol=https;branch=${SRCBRANCH} \
          "

S = "${WORKDIR}/git"

FILES_${PN} += " \
                   /lib/systemd/system/move-mounts@.service \
                   /lib/systemd/system-generators/liveboot-generator \
               "

FILES_${PN}-dbg += " \
                     /lib/systemd/system-generators/.debug/liveboot-generator \
                   "
