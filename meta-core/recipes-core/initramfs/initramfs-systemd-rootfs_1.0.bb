DESCRIPTION = "A rootfs initramfs framework using systemd"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "\
                   busybox \
                   systemd-initramfs \
                   aufs-util \
                   udev \
                   base-passwd \
                   live-boot-generator \
                 "
RRECOMMENDS_${PN} += "\
                        kernel-module-aufs \
                        kernel-module-squashfs \
                     "
PR = "r2"

do_install() {
    # let systemd know that it is in initrd mode.
    # http://www.freedesktop.org/wiki/Software/systemd/InitrdInterface/
    install -d ${D}/etc
    touch ${D}/etc/initrd-release
    
    # create the obvious, to be sure.
    install -d ${D}/dev
    install -d ${D}/proc
    install -d ${D}/run
    install -d ${D}/sys
    install -d ${D}/mnt
    install -d ${D}/tmp
    install -d ${D}/var/lock
    
    # mount point of rootfs for switch_root
    install -d ${D}/sysroot
}

FILES_${PN} += "\
    /etc \
    /dev \
    /lib \
    /mnt \
    /proc \
    /run \
    /sys \
    /tmp \
    /var \
    /sysroot \
    "
