DESCRIPTION = "A rootfs initramfs framework using systemd"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PACKAGE_INSTALL = " \
                  initramfs-systemd-rootfs \
                "

export IMAGE_BASENAME = "initramfs-systemd-image"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
inherit image

BAD_RECOMMENDATIONS += "busybox-syslog"

EXTRA_IMAGEDEPENDS = ""
IMAGE_FEATURES = "debug-tweaks"
IMAGE_LINGUAS = ""
IMAGE_LOGIN_MANAGER = ""
IMAGE_INIT_MANAGER = "systemd"
IMAGE_INITSCRIPTS = ""
IMAGE_DEV_MANAGER = "udev"
FEED_DEPLOYDIR_BASE_URI = ""
LDCONFIGDEPEND = ""

rootfs_initrd_configure() {

    # setup default target as initrd
    rm -f ${IMAGE_ROOTFS}/etc/systemd/system/default.target
    ln -s /lib/systemd/system/initrd.target ${IMAGE_ROOTFS}/etc/systemd/system/default.target
    mkdir -p ${IMAGE_ROOTFS}/etc/systemd/system/default.target.wants
    
    # clear out fstab
    echo "" > ${IMAGE_ROOTFS}/etc/fstab

    # remove units we do not want    
    rm -rf ${IMAGE_ROOTFS}/etc/systemd/system/systemd-random-seed.service.wants
    rm -rf ${IMAGE_ROOTFS}/etc/systemd/system/local-fs.target.wants
    rm -rf ${IMAGE_ROOTFS}/etc/systemd/system/multi-user.target.wants
    rm -rf ${IMAGE_ROOTFS}/etc/systemd/system/sysinit.target.wants

    ln -s /dev/null ${IMAGE_ROOTFS}/etc/systemd/system/systemd-fsck-root.service
    ln -s /dev/null ${IMAGE_ROOTFS}/etc/systemd/system/var-volatile-lib.service

    # we are not a real image
    rm -f ${IMAGE_ROOTFS}/etc/os-release    
}

rootfs_post_cleanup() {

    # remove kernel if present. initramfs does not need the kernel
    rm -rf ${IMAGE_ROOTFS}/boot
    # remove udev hardware db's as we don't need the names of devices.
    rm -f ${IMAGE_ROOTFS}/etc/udev/hwdb.bin
    rm -f ${IMAGE_ROOTFS}/lib/udev/hwdb.d/*
}

ROOTFS_POSTPROCESS_COMMAND += " rootfs_initrd_configure; rootfs_post_cleanup; "
