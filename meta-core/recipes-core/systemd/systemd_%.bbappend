FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

do_install_append() {
    # Don't overflow the console with messages, it is very difficult to use
    # the system as the systemd-timesyncd prints too much data

    sed -i "s/#LogTarget=journal-or-kmsg/LogTarget=journal/" ${D}${sysconfdir}/systemd/system.conf
    
    # disable tmp.mount. It was starting too late and we don't need it.
    ln -sf /dev/null ${D}/${systemd_unitdir}/system/tmp.mount

}

