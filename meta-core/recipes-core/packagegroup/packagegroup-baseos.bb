LICENSE = "MIT"
PR = "r0"

inherit packagegroup

RDEPENDS_${PN} = "\
    livepackage-generator \
    kernel-modules \
    packagegroup-baseos-firmware \
    ca-certificates \
    liberation-fonts \
    ttf-dejavu-common \
    ttf-dejavu-sans \
    tzdata \
    tzdata-americas \
    tzdata-asia \
    tzdata-europe \
    connman connman-client \
    qtimageformats-plugins qtsvg \
    cloudframe \
    pam-plugin-loginuid \
    haveged \
    "

