inherit update-alternatives

ALTERNATIVE_${PN} = "x-window-manager"
ALTERNATIVE_LINK[x-window-manager] = "${bindir}/x-window-manager"
ALTERNATIVE_TARGET[x-window-manager] = "${bindir}/matchbox-window-manager"

