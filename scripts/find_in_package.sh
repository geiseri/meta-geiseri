#!/bin/bash

for PKG in $(find ${1} -type f -name "*.ipk")
do
   RESULT=$(dpkg-deb -c ${PKG} | grep ${2} | awk '{print $6}')
   if [ "${RESULT}" != "" ]; then
      echo "${PKG} -> ${RESULT}"
   fi
done

